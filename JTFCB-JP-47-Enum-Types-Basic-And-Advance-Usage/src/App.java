// enum
public class App {

	// to znika po dodaniu Enum
	// public static final int DOG = 0;
	// public static final int CAT = 1;
	// public static final int MOUSE = 2;

	public static void main(String[] args) {

		// zamiast tego moge zrobi� to ponizej
		// int animal = CAT;
		Animal animal = Animal.DOG;

		switch (animal) {
		// mozna wygenerowa� po kliknieciu w zolty error
		case CAT:
			System.out.println("Cat");
			break;
		case DOG:
			System.out.println("Dog");
			break;
		case MOUSE:
			break;
		default:
			break;

		// to zmaza�
		// case DOG:
		// System.out.println("Dog");
		// break;

		// case CAT:
		// System.out.println("Cat");
		// break;

		// mozna wygenerowa�
			
			

		}
		
		System.out.println(Animal.DOG);
		// System.out.println("Enum name as a string " + Animal.DOG.name());
		System.out.println(Animal.DOG.getClass());
		// System.out.println(Animal.DOG instanceof Animal);
		// System.out.println(Animal.MOUSE.getName());
		
	}

}
